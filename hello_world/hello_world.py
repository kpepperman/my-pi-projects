#! /usr/bin/python

# Our first Hello Word on the Raspberry Pi!
import time

print("Hello World from a Raspberry Pi")
print("")
print ("Sleeping for 5 seconds")
time.sleep (5)

#Print another new line
print("")

print("Exiting...")