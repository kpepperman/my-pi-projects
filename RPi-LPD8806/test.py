#!/usr/bin/python

from time import sleep
from LPD8806 import *

num = 64;
led = LEDStrip(num)
#led.setChannelOrder(ChannelOrder.BRG) #Only use this if your strip does not use the GRB order
#led.setMasterBrightness(0.5) #use this to set the overall max brightness of the strip
led.setMasterBrightness(1.0)
led.all_off()

while True:
    for i in range(384*2):
        led.anim_rainbow_cycle()
        led.update()
    
    
    led.fillOff()
    

