#!/usr/bin/python

from time import sleep
from LPD8806 import *

num = 64;
led = LEDStrip(num)
#led.setChannelOrder(ChannelOrder.BRG) #Only use this if your strip does not use the GRB order
#led.setMasterBrightness(0.5) #use this to set the overall max brightness of the strip
led.all_off()


#led.fill(Color(255.0,0.0,255.0, 0.5))

#led.fill(Color(255.0,0.0,255.0, 0.5))

led.fill(Color(255.0,255.0,1.0, 0.8))
led.update()