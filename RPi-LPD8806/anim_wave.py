#!/usr/bin/python

from time import sleep
from LPD8806 import *

num = 64;
led = LEDStrip(num)
#led.setChannelOrder(ChannelOrder.BRG) #Only use this if your strip does not use the GRB order
#led.setMasterBrightness(0.5) #use this to set the overall max brightness of the strip
led.setMasterBrightness(0.5)
led.all_off()

color = Color(255, 255, 0)

while True:

   # for i in range(led.lastIndex):
   #     led.anim_wave(color, 4)
   #     led.update()
   #     sleep(0.15)

#rolling rainbow
    for i in range(384):
        led.anim_rainbow()
        led.update()
    
    led.fillOff()


#evenly distributed rainbow
    for i in range(384*4):
        led.anim_rainbow_cycle()
        led.update()
        

    led.fillOff()

    #rolling rainbow
    for i in range(384):
        led.anim_rainbow()
        led.update()
        
	
    led.fillOff()
